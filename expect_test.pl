#!/usr/bin/perl

use strict;
use warnings;
use Expect;
use Test::More tests => 2;
use Getopt::Std;

my %opt=();
getopts("h:p:u:", \%opt) and defined($opt{h}) and defined($opt{u}) or
    die "Usage: [-u <user>] [-p <password>] -h <host>\n";

my $user_host = "$opt{u}\@$opt{h}";

my $exp = Expect->new;
$exp->raw_pty(1);
my $timeout = 20 ;

sub test_ssh_copy_id($) {
  my $command = shift;

  $exp = Expect->spawn($command)
      or die "Cannot spawn ssh-copy-id: $!\n";

  my $spawn_ok = 0;
  my ($matched_pattern_position, $error, $successfully_matching_string, $before_match,
      $after_match) = "";
  ($matched_pattern_position, $error, $successfully_matching_string,
   $before_match, $after_match) =
      $exp->expect($timeout,
                   ['INFO: ', sub {$spawn_ok = 1; exp_continue;}],
                   ['assword: ', sub {my $self = shift; $self->send("$opt{p}\n") ; exp_continue} ],
                   ['Number of key\(s\) added:', sub {note("    found: keys added")}],
                   ['WARNING: All keys were skipped', sub {note("    found: All skipped")}],
                   [eof =>
                    sub {
                      if ($spawn_ok) {
                        diag("ERROR: premature EOF in login.");
                      } else {
                        diag("ERROR: could not spawn ssh-copy-id.");
                      }
                    }
                   ],
                   [
                    timeout =>
                    sub {
                      diag("No login.");
                    }
                   ]
      );

  note ("result: ", $matched_pattern_position, " match [", $successfully_matching_string, "] after match [", $after_match, "]") ;
  $exp->soft_close() ;
  return $matched_pattern_position;
}

my $extra_args = '' ;

# FIXME -- this is a kludge -- need to support -i option
$extra_args = $ARGV[0] if defined($ARGV[0]) ;

is(test_ssh_copy_id("./ssh-copy-id " . $extra_args . " $user_host"), 3, "keys added");
is(test_ssh_copy_id("./ssh-copy-id " . $extra_args . " $user_host"), 4, "All keys skipped");
